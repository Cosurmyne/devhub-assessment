package bcx.devhub.rest.web;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;

import java.net.URI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.util.Collection;
import bcx.devhub.rest.domain.HttpResponse;
import bcx.devhub.rest.domain.model.User;
import bcx.devhub.rest.domain.dto.UserDTO;
import bcx.devhub.rest.domain.results.Employees;
import bcx.devhub.rest.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/employee")
public class EmployeeController {

    private final UserService empService;

    EmployeeController(UserService userService){
	this.empService = userService;
    }

    @Value("${spring.client.web}")
    private String web;

    @GetMapping("/ls/{page}/{size}")
    public Employees ls(@PathVariable int page, @PathVariable int size) {
        return this.empService.ls(page, size);
    }

    @GetMapping("/id/{id}")
    public UserDTO get(
    @PathVariable String id
    ) {
        return this.empService.byId(id);
    }

    @PostMapping("/crud")
    public int crud(@RequestBody User payload) {
    return empService.crud(payload);
  }

  @DeleteMapping("/{id}")
  public int delete(@PathVariable String id){
    return empService.delete(id);
  }

}
