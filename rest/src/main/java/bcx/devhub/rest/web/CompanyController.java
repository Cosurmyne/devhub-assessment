package bcx.devhub.rest.web;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;

import java.net.URI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.util.Collection;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import bcx.devhub.rest.domain.HttpResponse;
import bcx.devhub.rest.domain.model.Company;
import bcx.devhub.rest.domain.dto.CompanyDTO;
import bcx.devhub.rest.domain.results.Companies;
import bcx.devhub.rest.service.CompanyService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/company")
public class CompanyController {

    private final CompanyService comService;

    CompanyController(CompanyService companyService){
	this.comService = companyService;
    }

    @Value("${spring.client.web}")
    private String web;

    @GetMapping("/id/{id}")
    public CompanyDTO get(@PathVariable int id) {
        return this.comService.byId(id);
    }
    @GetMapping("/ls/{page}/{size}")
    public Companies ls(@PathVariable int page, @PathVariable int size) 
    {
        return this.comService.ls(page, size);
    }

    @PostMapping("/crud")
    public int crud(@RequestBody Company payload) 
    {
    	return comService.crud(payload);
    }

  @DeleteMapping("/{id}")
  public int delete(@PathVariable int id){
    return comService.delete(id);
  }

}
