package bcx.devhub.rest.service.base;

import java.util.Collection;
import bcx.devhub.rest.domain.dto.CompanyDTO;
import bcx.devhub.rest.domain.model.Company;
import bcx.devhub.rest.domain.results.Companies;

public interface CompanyServiceInterface {
    int crud(Company data);
    int delete(int id);
    CompanyDTO byId(int id);
    Companies ls(int page, int size);
}
