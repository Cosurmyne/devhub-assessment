package bcx.devhub.rest.service.base;

import java.util.Collection;
import bcx.devhub.rest.domain.dto.UserDTO;
import bcx.devhub.rest.domain.model.User;
import bcx.devhub.rest.domain.results.Employees;

public interface UserServiceInterface {
    int crud(User user);
    int delete(String id);
    UserDTO byId(String id);
    UserDTO byEmail(String email);
    Employees ls(int page, int size);
}
