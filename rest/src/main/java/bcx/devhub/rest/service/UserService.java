package bcx.devhub.rest.service;

import java.util.Collection;
import org.springframework.stereotype.Service;
import bcx.devhub.rest.domain.dto.UserDTO;
import bcx.devhub.rest.domain.mapper.UserDTOMapper;
import bcx.devhub.rest.domain.model.User;
import bcx.devhub.rest.domain.results.Employees;
import bcx.devhub.rest.repo.base.UserRepoInterface;
import bcx.devhub.rest.service.base.UserServiceInterface;

@Service
public class UserService implements UserServiceInterface {

    private final UserRepoInterface<User> userRepo;

    UserService(UserRepoInterface<User> userRepo){
    	this.userRepo = userRepo;
    }

    @Override
    public int delete(String id){
        return userRepo.delete(id);
    }

    @Override
    public int crud(User user) {
        return userRepo.crud(user);
    }

    @Override
    public UserDTO byId(String id) {
        return UserDTOMapper.fromUser(this.userRepo.get(id));
    }

    @Override
    public UserDTO byEmail(String email) {
        return userRepo.getByEmail(email);
    }

    @Override
    public Employees ls(int page, int size) {
	long total = this.userRepo.count();
        Collection<UserDTO> list = this.userRepo.list(page, size);
        return new Employees(list,total);
    }
}
