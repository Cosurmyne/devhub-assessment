package bcx.devhub.rest.service;

import java.util.Collection;
import org.springframework.stereotype.Service;
import bcx.devhub.rest.domain.dto.CompanyDTO;
import bcx.devhub.rest.domain.mapper.CompanyDTOMapper;
import bcx.devhub.rest.domain.model.Company;
import bcx.devhub.rest.domain.results.Companies;
import bcx.devhub.rest.repo.base.CompanyRepoInterface;
import bcx.devhub.rest.service.base.CompanyServiceInterface;

@Service
public class CompanyService implements CompanyServiceInterface {

    private final CompanyRepoInterface<Company> comRepo;

    CompanyService(CompanyRepoInterface<Company> comRepo){
	this.comRepo = comRepo;
    }
    @Override
    public int crud(Company data){
        return comRepo.crud(data);
    }

    @Override
    public int delete(int id){
        return comRepo.delete(id);
    }

    @Override
    public CompanyDTO byId(int id) {
        return CompanyDTOMapper.fromCompany(this.comRepo.get(id));
    }

    @Override
    public Companies ls(int page, int size) {
	long total = this.comRepo.count();
        Collection<CompanyDTO> list = this.comRepo.list(page, size);
        return new Companies(list,total);
    }
}
