package bcx.devhub.rest.domain.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class UserDTO {

    private String id;
    private String firstname;
    private String lastname;
    private String idnumber;
    private String email;
    private String phone;
    private String photo;
    private short active;
    private Date dob;
    private String title;
    private String nationality;
    private Timestamp modified;
    private Timestamp created;
    private char roleId;
    private String roleName;

    public UserDTO setId(String id) {
        this.id = id;
        return this;
    }

    public UserDTO setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public UserDTO setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public UserDTO setIdnumber(String idnumber) {
        this.idnumber = idnumber;
        return this;
    }

    public UserDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserDTO setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserDTO setPhoto(String photo) {
        this.photo = photo;
        return this;
    }

    public UserDTO setActive(short active) {
        this.active = active;
        return this;
    }

    public UserDTO setDob(Date dob) {
        this.dob = dob;
        return this;
    }

    public UserDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public UserDTO setNationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public UserDTO setModified(Timestamp modified) {
        this.modified = modified;
        return this;
    }

    public UserDTO setCreated(Timestamp created) {
        this.created = created;
        return this;
    }

    public UserDTO setRoleId(char roleId) {
        this.roleId = roleId;
        return this;
    }

    public UserDTO setRoleName(String roleName) {
        this.roleName = roleName;
        return this;
    }

    public String getId() {
        return this.id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getIdnumber() {
        return this.idnumber;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getPhoto() {
        return this.photo;
    }

    public short getActive() {
        return this.active;
    }

    public Date getDob() {
        return this.dob;
    }

    public String getTitle() {
        return this.title;
    }

    public String getNationality() {
        return this.nationality;
    }

    public Timestamp getModified() {
        return this.modified;
    }

    public Timestamp getCreated() {
        return this.created;
    }

    public char getRoleId() {
        return this.roleId;
    }

    public String getRoleName() {
        return this.roleName;
    }
}
