package bcx.devhub.rest.domain.results;
import java.util.Collection;
import bcx.devhub.rest.domain.dto.UserDTO;

public class Employees {
	public long total;
	public Collection<UserDTO> employees;
	public Employees(Collection<UserDTO> employees, long total){
		this.employees = employees;
		this.total = total;
	}
}

