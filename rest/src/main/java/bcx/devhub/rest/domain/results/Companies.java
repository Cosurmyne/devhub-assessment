package bcx.devhub.rest.domain.results;
import java.util.Collection;
import bcx.devhub.rest.domain.dto.CompanyDTO;

public class Companies {
	public long total;
	public Collection<CompanyDTO> companies;
	public Companies(Collection<CompanyDTO> companies, long total){
		this.companies = companies;
		this.total = total;
	}
}
