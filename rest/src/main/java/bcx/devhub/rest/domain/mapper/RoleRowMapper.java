package bcx.devhub.rest.domain.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import bcx.devhub.rest.domain.model.Role;

public class RoleRowMapper implements RowMapper<Role> {

    @Override
    public Role mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Role
            ()
            .setId(resultSet.getString("role_id").charAt(0))
            .setName(resultSet.getString("name"))
            .setDescription(resultSet.getString("description"))
            ;
    }
}
