package bcx.devhub.rest.domain.mapper;

import bcx.devhub.rest.domain.dto.CompanyDTO;
import bcx.devhub.rest.domain.model.Company;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class CompanyDTOMapper {

    public static CompanyDTO fromCompany(Company company) {
        CompanyDTO dto = new CompanyDTO();
        BeanUtils.copyProperties(company, dto);
        return dto;
    }

    public static Company toCompany(CompanyDTO dto) {
        Company company = new Company();
        BeanUtils.copyProperties(dto, company);
        return company;
    }
}
