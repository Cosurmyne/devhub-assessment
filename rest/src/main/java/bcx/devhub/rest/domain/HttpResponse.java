package bcx.devhub.rest.domain;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_DEFAULT;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Map;
import org.springframework.http.HttpStatus;

@JsonInclude(NON_DEFAULT)
public class HttpResponse {

    protected String timeStamp;
    protected String message;
    protected HttpStatus status;
    protected Map<?, ?> data;
}
