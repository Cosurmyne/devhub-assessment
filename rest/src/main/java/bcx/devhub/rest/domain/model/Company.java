package bcx.devhub.rest.domain.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_DEFAULT;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(NON_DEFAULT)
public class Company {
    private int id;
    private String name;
    private String address;

    public int getId(){ return this.id; }
    public String getName(){ return this.name;}
    public String getAddress(){ return this.address;}

    public Company setId(int id){ this.id = id; return this; }
    public Company setName(String name){ this.name = name;return this;}
    public Company setAddress(String address){ this.address = address; return this;}
}
