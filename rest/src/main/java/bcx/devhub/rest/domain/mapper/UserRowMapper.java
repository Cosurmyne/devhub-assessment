package bcx.devhub.rest.domain.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import bcx.devhub.rest.domain.model.User;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        return new User()
            .setId(resultSet.getString("user_id"))
            .setFirstname(resultSet.getString("firstname"))
            .setLastname(resultSet.getString("lastname"))
            .setIdnumber(resultSet.getString("idnumber"))
            .setEmail(resultSet.getString("email"))
            .setPhone(resultSet.getString("phone"))
            .setPassword(resultSet.getString("password"))
            .setPhoto(resultSet.getString("photo"))
            .setActive(resultSet.getShort("active"))
            .setDob(resultSet.getDate("dob"))
            .setTitle(resultSet.getString("title"))
            .setNationality(resultSet.getString("nationality"))
            .setModified(resultSet.getTimestamp("modified"))
            .setCreated(resultSet.getTimestamp("created"))
            .setRoleId(resultSet.getString("role_id").charAt(0))
            ;
    }
}
