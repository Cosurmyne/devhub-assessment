package bcx.devhub.rest.domain.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import bcx.devhub.rest.domain.dto.CompanyDTO;

public class CompanyDTORowMapper implements RowMapper<CompanyDTO> {

    @Override
    public CompanyDTO mapRow(ResultSet resultSet, int i) throws SQLException {
        return (new CompanyDTO()).setId(resultSet.getInt("com_id"))
            .setName(resultSet.getString("name"))
            .setAddress(resultSet.getString("address"))
            ;
    }
}
