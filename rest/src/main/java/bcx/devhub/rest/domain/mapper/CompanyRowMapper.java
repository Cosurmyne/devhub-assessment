package bcx.devhub.rest.domain.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import bcx.devhub.rest.domain.model.Company;

public class CompanyRowMapper implements RowMapper<Company> {

    @Override
    public Company mapRow(ResultSet resultSet, int i) throws SQLException {
        return  new Company
            ()
            .setId(resultSet.getString("com_id").charAt(0))
            .setName(resultSet.getString("name"))
            .setAddress(resultSet.getString("address"))
            ;
    }
}
