package bcx.devhub.rest.domain.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class CompanyDTO {

    private int id;
    private String name;
    private String address;

    public int getId(){ return this.id; }
    public String getName(){ return this.name;}
    public String getAddress(){ return this.address;}

    public CompanyDTO setId(int id){ this.id = id; return this; }
    public CompanyDTO setName(String name){ this.name = name;return this;}
    public CompanyDTO setAddress(String address){ this.address = address; return this;}
}
