package bcx.devhub.rest.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;

import java.sql.Date;
import java.sql.Timestamp;

import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_DEFAULT;
@JsonInclude(NON_DEFAULT)
public class User {

    private String id;

    @NotEmpty(message = "First Name is Required")
    private String firstname;

    @NotEmpty(message = "Last Name is Required")
    private String lastname;

    @NotEmpty(message = "ID Number is Required")
    private String idnumber;

    @NotEmpty(message = "Email Address is Required")
    @Email(message = "Invalid Email Address")
    private String email;

    private String phone;

    @NotEmpty(message = "Password is Required")
    private String password;

    private String photo;
    private short active;
    private Date dob;
    private String title;
    private String nationality;
    private Timestamp modified;
    private Timestamp created;
    private char roleId;

    public String getId(){ return this.id; }
    public String getFirstname(){ return this.firstname; }
    public String getLastname() { return this.lastname; }
    public String getIdnumber(){return this.idnumber;}
    public String getEmail(){return this.email;}
    public String getPhone(){ return this.phone; }
    public String getPassword(){ return this.password; }
    public String getPhoto(){ return this.photo; }
    public short getActive() { return this.active; }
    public Date getDob(){ return this.dob; }
    public String getTitle() { return this.title; }
    public String getNationality() { return this.nationality; }
    public Timestamp getModified() { return this.modified; }
    public Timestamp getCreated() { return this.created; }
    public char getRoleId() { return this.roleId; }

    public User setId(String id){ this.id = id; return this; }
    public User setFirstname(String firstname){ this.firstname = firstname; return this; }
    public User setLastname(String lastname) { this.lastname = lastname; return this; }
    public User setIdnumber(String idnumber){this.idnumber = idnumber; return this;}
    public User setEmail(String email){this.email = email; return this;}
    public User setPhone(String phone){ this.phone = phone; return this; }
    public User setPassword(String password){ this.password = password; return this; }
    public User setPhoto(String photo){ this.photo = photo;return this; }
    public User setActive(short active) { this.active = active;return this; }
    public User setDob(Date dob){ this.dob = dob;return this; }
    public User setTitle(String title) { this.title = title;return this; }
    public User setNationality(String nationality) { this.nationality = nationality;return this; }
    public User setModified(Timestamp modified) { this.modified = modified;return this; }
    public User setCreated(Timestamp created) { this.created = created; return this; }
    public User setRoleId(char roleId) { this.roleId = roleId; return this; }
}
