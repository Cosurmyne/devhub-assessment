package bcx.devhub.rest.domain.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_DEFAULT;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(NON_DEFAULT)
public class Role {

    private char id;
    private String name;
    private String description;

    public char getId(){ return this.id; }
    public String getName(){ return this.name;}
    public String getDescription(){ return this.description;}

    public Role setId(char id){ this.id = id; return this; }
    public Role setName(String name){ this.name = name;return this;}
    public Role setDescription(String description){ this.description = description; return this;}
}
