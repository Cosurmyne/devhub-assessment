package bcx.devhub.rest.repo.base;

import static java.util.Map.of;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.HashMap;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.lang.Nullable;

public class AbstractRepo {

    private final NamedParameterJdbcTemplate jdbc;
    public AbstractRepo(NamedParameterJdbcTemplate jdbc){
	this.jdbc = jdbc;
    }

    @Autowired
    private DataSource dataSource;

    public Map<String, Object> sp(
        String name,
        SqlParameterSource param,
        RowMapper<?> mapper
    ) {
        Map<String, Object> out = null;
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(this.dataSource)
                .withProcedureName(name)
                .returningResultSet("result", mapper);

            out = jdbcCall.execute(param);
            Integer code = (Integer) out.get("code");
            String info = ((String) out.get("info"));
            if (code != 0) {
                throw new RuntimeException(info);
            }
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return out;
    }

    public Collection query(
        String sql,
        SqlParameterSource param,
        RowMapper<?> mapper
    ) {
        try {
            return this.jdbc.query(sql, param, mapper);
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public <T> T query(String sql, Map<String, ?> param, RowMapper<T> mapper) {
        try {
            return this.jdbc.queryForObject(sql, param, mapper);
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public int dml(String sql, SqlParameterSource args) {
        try {
            return this.jdbc.update(sql, args);
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    public int dml(String sql, @Nullable Map<String, ?> args) {
        try {
            return this.jdbc.update(sql, args);
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public long count(String table){
        try {
            String sql = "SELECT COUNT(*) FROM " + table;
	    return this.jdbc.queryForObject(sql, (HashMap) null, Long.class);
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
