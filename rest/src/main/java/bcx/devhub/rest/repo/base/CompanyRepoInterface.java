package bcx.devhub.rest.repo.base;

import bcx.devhub.rest.domain.model.Company;
import bcx.devhub.rest.domain.dto.CompanyDTO;
import java.util.Collection;

public interface CompanyRepoInterface<T extends Company> {
    /* Basic CRUD Operations */
    Collection<CompanyDTO> list(int page, int pageSize);
    long count();
    int crud(T data);
    T get(int id);
    int delete(int id);
}
