package bcx.devhub.rest.repo.base;

import java.util.Collection;
import bcx.devhub.rest.domain.dto.UserDTO;
import bcx.devhub.rest.domain.model.User;

public interface UserRepoInterface<T extends User> {
    /* Basic CRUD Operations */
    Collection<UserDTO> list(int page, int pageSize);
    long count();
    int crud(T data);
    T get(String id);
    UserDTO getByEmail(String email);
    int delete(String id);
    /* More Complex Operations */
}
