package bcx.devhub.rest.repo;


import static java.util.Map.of;
import java.sql.Types;
import java.util.Collection;
import java.util.List;
import java.util.HashMap;

import org.springframework.stereotype.Repository;

import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.dao.EmptyResultDataAccessException;

import bcx.devhub.rest.domain.model.Company;
import bcx.devhub.rest.domain.dto.CompanyDTO;
import bcx.devhub.rest.domain.mapper.CompanyRowMapper;
import bcx.devhub.rest.domain.mapper.CompanyDTORowMapper;
import bcx.devhub.rest.repo.base.CompanyRepoInterface;
import bcx.devhub.rest.repo.base.AbstractRepo;

@Repository
public class CompanyRepo extends AbstractRepo implements CompanyRepoInterface<Company> {

    CompanyRepo (NamedParameterJdbcTemplate jdbc){
    	super(jdbc);
    }

    /* Basic CRUD Operations */
    @Override
    public long count(){
	return this.count("company");
    }

    @Override
    public Collection<CompanyDTO> list(int page, int pageSize) {
        String sql = "SELECT * FROM `company` LIMIT :size OFFSET :page";
        MapSqlParameterSource param = new MapSqlParameterSource()
            .addValue("page", page * pageSize, Types.INTEGER)
            .addValue("size", pageSize, Types.INTEGER);
	return this.query(sql, param, new CompanyDTORowMapper());
    }

    @Override
    public int crud(Company data) {
	String sql = "INSERT INTO `company`(name,address) values(:cname,:place)";
	if(data.getId() > 0)
	sql = "UPDATE `company` SET name=:cname,address=:place WHERE com_id = :cid";
	return this.dml(sql, getSqlParameterSource(data));
    }

    @Override
    public Company get(int id) {
        String sql = "SELECT * FROM `company` WHERE com_id = :cid";
	return this.query(sql, of("cid", id), new CompanyRowMapper());
    }

    @Override
    public int delete(int id) {
	String sql = "DELETE FROM `company` WHERE com_id = :id";
	return this.dml(sql, of("id", id));
    }

    private SqlParameterSource getSqlParameterSource(Company data) {
        return new MapSqlParameterSource()
            .addValue("cid", data.getId(), Types.INTEGER)
            .addValue("cname", data.getName(), Types.VARCHAR)
            .addValue("place", data.getAddress(), Types.VARCHAR);
    }
}
