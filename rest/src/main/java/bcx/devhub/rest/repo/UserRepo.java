package bcx.devhub.rest.repo;

import static java.util.Map.of;

import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import bcx.devhub.rest.domain.mapper.UserRowMapper;
import bcx.devhub.rest.domain.mapper.UserDTORowMapper;
import bcx.devhub.rest.domain.model.Role;
import bcx.devhub.rest.domain.model.User;
import bcx.devhub.rest.domain.dto.UserDTO;
import bcx.devhub.rest.repo.base.RoleRepoInterface;
import bcx.devhub.rest.repo.base.UserRepoInterface;
import bcx.devhub.rest.repo.base.AbstractRepo;

@Repository
public class UserRepo extends AbstractRepo implements UserRepoInterface<User> {

    private final RoleRepoInterface<Role> roleRepo;

    UserRepo(NamedParameterJdbcTemplate jdbc, RoleRepoInterface<Role> roleRepo){
	super(jdbc);
    	this.roleRepo = roleRepo;
    }


    /* Basic CRUD Operations */
    @Override
    public long count(){
	return this.count("user");
    }

    @Override
    public int crud(User payload) {
	String sql = "INSERT INTO user(firstname,lastname,email,idnumber,nationality) values(:fname,:lname,:umail,:idno,:nation)";
	if(!payload.getId().isEmpty())
		sql = "UPDATE user SET firstname=:fname,lastname=:lname,email=:umail,idnumber=:idno,nationality=:nation WHERE user_id = :uid";
	return this.dml(sql, getSqlParameterSource(payload.setNationality("ZA")));
    }

    @Override
    public Collection<UserDTO> list(int page, int pageSize) {
        String sql = "SELECT u.*, r.name role_name FROM user u LEFT JOIN role r USING(role_id) LIMIT :size OFFSET :page";
        MapSqlParameterSource param = new MapSqlParameterSource()
            .addValue("page", page * pageSize, Types.INTEGER)
            .addValue("size", pageSize, Types.INTEGER);
	return this.query(sql, param, new UserDTORowMapper());
    }

    @Override
    public User get(String id) {
        String sql = "SELECT * FROM user WHERE user_id = :uid";
	return this.query(sql, of("uid", id), new UserRowMapper());
    }

    @Override
    public UserDTO getByEmail(String email) {
         String sql = "SELECT u.*, r.name role_name FROM user u LEFT JOIN role r USING(role_id) WHERE email = :umail";
	return this.query(sql, of("umail", email), new UserDTORowMapper());
    }

    @Override
    public int delete(String id) {
	String sql = "DELETE FROM user WHERE user_id = :id";
	return this.dml(sql, of("id", id));
    }

    private SqlParameterSource getSqlParameterSource(User user) {
        return new MapSqlParameterSource()
            .addValue("uid", user.getId(), Types.CHAR)
            .addValue("fname", user.getFirstname(), Types.VARCHAR)
            .addValue("lname", user.getLastname(), Types.VARCHAR)
            .addValue("idno", user.getIdnumber(), Types.VARCHAR)
            .addValue("umail", user.getEmail(), Types.VARCHAR)
            .addValue("uphone", user.getPhone(), Types.VARCHAR)
            .addValue(
                "passwd",
                user.getPassword(),
                Types.CHAR
            )
            .addValue("passwd", user.getPassword(), Types.CHAR)
            .addValue("pic", user.getPhoto(), Types.VARCHAR)
            .addValue("state", user.getActive(), Types.TINYINT)
            .addValue("birth", user.getDob(), Types.DATE)
            .addValue("credential", user.getTitle(), Types.VARCHAR)
            .addValue("nation", user.getNationality(), Types.CHAR)
            .addValue("role", user.getRoleId(), Types.CHAR);
    }
}
