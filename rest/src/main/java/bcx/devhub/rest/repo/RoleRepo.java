package bcx.devhub.rest.repo;

import java.util.Collection;

import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import bcx.devhub.rest.domain.model.Role;
import bcx.devhub.rest.repo.base.RoleRepoInterface;
import bcx.devhub.rest.repo.base.AbstractRepo;


@Repository
public class RoleRepo extends AbstractRepo implements RoleRepoInterface<Role> {

    RoleRepo(NamedParameterJdbcTemplate jdbc){
	super(jdbc);
    }

    /* Basic CRUD Operations */
    @Override
    public long count(){
	return this.count("role");
    }

    @Override
    public Role crud(Role data) {
        return null;
    }

    @Override
    public Collection<Role> list(int page, int pageSize) {
        return null;
    }

    @Override
    public Role get(String id) {
        return null;
    }

    @Override
    public boolean delete(String id) {
        return true;
    }

    /* More Complex Operations */
    @Override
    public boolean updateUserRole(String userId, char roleId) {
        return true;
    }

    @Override
    public Role getRoleByUserId(String userId) {
        return null;
    }

    @Override
    public Role getRoleByUserEmail(String email) {
        return null;
    }
}
