package bcx.devhub.rest.repo.base;

import bcx.devhub.rest.domain.model.Role;
import java.util.Collection;

public interface RoleRepoInterface<T extends Role> {
    /* Basic CRUD Operations */
    T crud(T data);
    long count();
    Collection<T> list(int page, int pageSize);
    T get(String id);
    boolean delete(String id);
    /* More Complex Operations */
    boolean updateUserRole(String userId, char roleId);
    Role getRoleByUserId(String userId);
    Role getRoleByUserEmail(String email);
}
