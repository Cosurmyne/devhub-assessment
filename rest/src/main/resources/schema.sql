--
-- Current Database: `devhubassessment`
--

DROP DATABASE IF EXISTS `devhubassessment`$$

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `devhubassessment` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */$$

USE `devhubassessment`$$

--
-- Table structure for table `account_log`
--

DROP TABLE IF EXISTS `account_log`$$
CREATE TABLE `account_log` (
  `log_id` char(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `type` varchar(15) NOT NULL,
  `colour` enum('primary','secondary','success','danger','warning','info','light','dark') DEFAULT NULL,
  `icon` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `type` (`type`),
  UNIQUE KEY `colour` (`colour`,`icon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$

--
-- Table structure for table `account_state`
--

DROP TABLE IF EXISTS `account_state`$$
CREATE TABLE `account_state` (
  `accst_id` tinyint(4) NOT NULL,
  `state` varchar(16) NOT NULL,
  `hint` varchar(35) DEFAULT NULL,
  `report` varchar(95) DEFAULT NULL,
  PRIMARY KEY (`accst_id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`$$
CREATE TABLE `role` (
  `role_id` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(15) NOT NULL,
  `description` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$

--
-- Table structure for table `tokendesc`
--

DROP TABLE IF EXISTS `tokendesc`$$
CREATE TABLE `tokendesc` (
  `tokentype_id` char(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` varchar(56) DEFAULT NULL,
  PRIMARY KEY (`tokentype_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$


--
-- Table structure for table `company`
--
DROP TABLE IF EXISTS `company`$$
CREATE TABLE company(com_id int unsigned auto_increment primary key, name varchar(20) not \N unique, address varchar(45))$$

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`$$
CREATE TABLE `user` (
  `user_id` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `idnumber` varchar(32) DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `password` char(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `photo` varchar(5) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `dob` date DEFAULT NULL,
  `title` enum('Mr.','Ms.','Mrs.','Dr.','Prof.','Rev.','Sir') DEFAULT NULL,
  `nationality` char(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `modified` datetime DEFAULT current_timestamp(),
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `role_id` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT 'G',
  `com_id` INT unsigned,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_id` (`user_id`,`role_id`),
  UNIQUE KEY `phone` (`phone`),
  KEY `active` (`active`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`active`) REFERENCES `account_state` (`accst_id`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`$$
CREATE TABLE `token` (
  `token_id` char(22) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `token` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `tokentype_id` char(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `user_id` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `time` datetime NOT NULL DEFAULT current_timestamp(),
  `useragent` text DEFAULT NULL,
  PRIMARY KEY (`token_id`),
  UNIQUE KEY `user_id` (`user_id`,`tokentype_id`),
  KEY `user` (`user_id`),
  KEY `type` (`tokentype_id`),
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `token_ibfk_2` FOREIGN KEY (`tokentype_id`) REFERENCES `tokendesc` (`tokentype_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`$$
CREATE TABLE `notification` (
  `user_id` char(12) NOT NULL,
  `notif_count` smallint(5) unsigned NOT NULL,
  `type` enum('alert','event','log') NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `message` varchar(255) NOT NULL,
  `time` datetime NOT NULL DEFAULT current_timestamp(),
  `read` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`,`notif_count`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci$$

--
-- Table structure for table `user_account_log`
--

DROP TABLE IF EXISTS `user_account_log`$$
CREATE TABLE `user_account_log` (
  `user_id` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `log_id` char(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `useragent` text DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp(),
  KEY `user_id` (`user_id`),
  KEY `log_id` (`log_id`),
  CONSTRAINT `user_account_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_account_log_ibfk_2` FOREIGN KEY (`log_id`) REFERENCES `account_log` (`log_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci$$

--
-- Dumping routines for database 'devhubassessment'
--
CREATE  FUNCTION `abbr`(string VARCHAR(255), length TINYINT UNSIGNED) RETURNS varchar(255) CHARSET utf8mb4 COLLATE utf8mb4_general_ci
    DETERMINISTIC
BEGIN
	DECLARE strlen SMALLINT UNSIGNED DEFAULT LENGTH(TRIM(string));
	DECLARE words SMALLINT UNSIGNED DEFAULT arrLength(' ', string);
	DECLARE difference SMALLINT DEFAULT CAST(length AS INT) - CAST(words AS INT);
	DECLARE y SMALLINT UNSIGNED DEFAULT 0;
	DECLARE abbr VARCHAR(255) DEFAULT '';
	DECLARE x, wordlen SMALLINT UNSIGNED;
	DECLARE word VARCHAR(255);
	DECLARE code TINYINT UNSIGNED;
	DECLARE iterations DOUBLE(16,2);

	IF length < 1 || strlen < 1 THEN RETURN \N; END IF;
	SET @iterations = '';

	WHILE y < words DO
		SET word = split(string, ' ', y);
		SET wordlen = LENGTH(word);
		SET x = 0;
		SET iterations = difference / length;
		

		inner_loop:
		WHILE x < wordlen DO
			SET code = ASCII(UPPER(SUBSTRING(word, x + 1, 1)));
			SET x = x + 1;
			IF code < 65 || code > 90 THEN ITERATE inner_loop; END IF;
			SET abbr = CONCAT(abbr, CHAR(code));
			IF difference = 0 THEN LEAVE inner_loop; END IF;
			SET difference = difference + IF(difference < 0, 1, -1);
			SET iterations = iterations + IF(difference < length, 1, -1);
		END WHILE;
		SET y = y + 1;
	END WHILE;

	RETURN abbr;
END $$

CREATE  FUNCTION `arrLength`(seperate VARCHAR(1), arrayString TEXT) RETURNS smallint(5) unsigned
BEGIN
	DECLARE count SMALLINT UNSIGNED DEFAULT 1;
	DECLARE string TEXT DEFAULT arrayString;

	WHILE (LOCATE(seperate, string) > 0)  DO
		SET string = SUBSTRING(string, LOCATE(seperate, string) + 1);
		SET count = count + 1;
	END WHILE;
 	RETURN count;
END $$

CREATE  FUNCTION `caste`(string VARCHAR(255),
	target CHAR(2) CHARSET UTF8
) RETURNS char(1) CHARSET utf8mb3 COLLATE utf8mb3_general_ci
BEGIN
	DECLARE item CHAR(1) CHARSET UTF8 DEFAULT 'G';
	SET string = TRIM(string);
	CASE target
	WHEN 'ZA' THEN
		IF LENGTH(string) < 13 THEN RETURN item; END IF;
		RETURN IF(SUBSTR(string, 7, 4) >= 5000, 'A', 'B');
	WHEN '--' THEN
		IF LENGTH(string) < 12 THEN RETURN item; END IF;
		RETURN SUBSTR(string, 6, 1);
	ELSE RETURN item;
	END CASE;
END $$

CREATE  FUNCTION `dob`(idno	VARCHAR(32),
	nation	CHAR(2),
	birthday DATE
) RETURNS date
BEGIN
	SET idno = TRIM(idno);

	CASE nation
	WHEN 'ZA' THEN
		IF idno REGEXP '^[0-9]{13}$' THEN
			SET birthday = STR_TO_DATE(SUBSTRING(idno,1,6),'%y%m%d');
		END IF;
	ELSE
	BEGIN
	END;
	END CASE;

	RETURN birthday;
END $$

CREATE  FUNCTION `luhnchk`(string VARCHAR(255)) RETURNS tinyint(1)
BEGIN
	DECLARE validsum INT DEFAULT 0;
	DECLARE k INT DEFAULT 1;
	DECLARE l INT DEFAULT LENGTH(string) - 1;
	DECLARE calck INT;
	IF NOT string REGEXP '^[0-9]+$' THEN RETURN FALSE; END IF;

	WHILE l >= 0 DO
		SET calck = CAST(SUBSTRING(string, l + 1, 1) AS INT) * k;
		IF calck > 9 THEN
			SET validsum = validsum + 1;
			SET calck = calck - 10;
		END IF;
		SET validsum = validsum + calck;
		IF  k = 1 THEN SET k = 2; ELSE SET k = 1; END IF;
		SET l = l - 1;
	END WHILE;

	RETURN (validsum % 10) = 0;
END $$

CREATE  FUNCTION `short`(string VARCHAR(255), length TINYINT UNSIGNED) RETURNS varchar(255) CHARSET utf8mb4 COLLATE utf8mb4_general_ci
    DETERMINISTIC
BEGIN
	DECLARE strlen SMALLINT UNSIGNED DEFAULT LENGTH(TRIM(string));
	DECLARE words SMALLINT UNSIGNED DEFAULT arrLength(' ', string);
	DECLARE size INT DEFAULT CAST(length / words AS INT);
	DECLARE chars VARCHAR(255) DEFAULT '';
	DECLARE word, piece VARCHAR(255);
	DECLARE y TINYINT UNSIGNED DEFAULT 0;
	DECLARE code, x, wordlen, i TINYINT UNSIGNED;
	DECLARE done, captured, desperate BOOLEAN;

	IF length < 1 || strlen < 1 THEN RETURN \N; END IF;

	WHILE y < length && y < words DO
		SET word = IFNULL(split(string, ' ', y), '#');
		SET piece = '';
		SET wordlen = LENGTH(word);
		SET x = 0;
		SET i = 0;
		SET done = FALSE;
		SET captured = FALSE;
		SET desperate = FALSE;

		WHILE x < wordlen && NOT done DO
			SET code = ASCII(SUBSTRING(word, x + 1, 1));
			CASE i
				WHEN 0 THEN
					IF (code >= 65 && code <= 90) || (code >= 97 && code <= 122) THEN
						SET piece = CONCAT(piece, CHAR(code));
						SET i = i + 1;
						SET captured = TRUE;
					END IF;
				ELSE
					IF (code >= 65 && code <= 90) || ((code > 97 && code <= 122) && desperate) THEN
						IF code <> 101 && code <> 105 && code <> 111 && code <> 117 THEN
							SET piece = CONCAT(piece, CHAR(code));
							SET i = i + 1;
						END IF;
					END IF;
					IF LENGTH(piece) < size && x = wordlen - 1 THEN
						SET desperate = TRUE;
						SET x = 0;
					END IF;
			END CASE;
			SET done = captured && i = size;
			SET x = x + 1;
		END WHILE;
		SET chars = CONCAT(chars, piece);
		SET y = y + 1;
	END WHILE;

	RETURN UPPER(chars);
END $$

CREATE  FUNCTION `split`(x TEXT, delim VARCHAR(1), pos SMALLINT UNSIGNED) RETURNS varchar(255) CHARSET utf8mb4 COLLATE utf8mb4_general_ci
    DETERMINISTIC
BEGIN
	DECLARE string VARCHAR(255) DEFAULT REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos + 1), LENGTH(SUBSTRING_INDEX(x, delim, pos)) + 1), delim, '');
	RETURN IF(LENGTH(string) = 0 , \N, string);
END $$

CREATE  PROCEDURE `crud_user`( IN uid char(12) CHARSET UTF8, IN fname varchar(20), IN lname varchar(20), IN idno varchar(32), IN umail varchar(254), IN uphone varchar(15), IN passwd char(60), IN pic varchar(5), IN state tinyint(1), IN birth date, IN credential enum('Mr.','Ms.','Mrs.','Dr.','Prof.','Rev.','Sir'), IN nation char(2), IN role char(1) CHARSET UTF8, OUT code INT, OUT info VARCHAR(255) )
BEGIN DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265 BEGIN GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT; ROLLBACK; END; START TRANSACTION; IF uid IS NULL THEN INSERT INTO user(firstname,lastname,idnumber,email,phone,password,photo,dob,title,nationality,role_id) VALUES(fname,lname,idno,umail,uphone,passwd,pic,birth,credential,nation,role); SET uid = @USER_ID; ELSE IF umail = '?' THEN DELETE FROM user WHERE user_id = uid; ELSE UPDATE user SET firstname = fname, lastname = lname, idnumber = idno, email = umail, phone = uphone, password = passwd, photo = pic, dob = birth, title = credential, nationality = nation, role_id = role WHERE user_id = uid; END IF; END IF; COMMIT; SELECT * FROM user WHERE user_id = uid; SET code = 0; SET info = uid; END $$

