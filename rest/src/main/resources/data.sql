-- MariaDB dump 10.19  Distrib 10.6.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: devhubassessment
-- ------------------------------------------------------
-- Server version	10.6.12-MariaDB-0ubuntu0.22.04.1
--
-- Dumping data for table `account_log`
--

INSERT INTO `account_log` VALUES ('FA','MFA Update',NULL,NULL),('IU','Info Update',NULL,NULL),('LF','Login Failure',NULL,NULL),('LS','Login Success',NULL,NULL),('PC','Password Change',NULL,NULL),('PU','Photo Update',NULL,NULL),('RU','Role Update',NULL,NULL),('SU','Settings Update',NULL,NULL)$$

--
-- Dumping data for table `account_state`
--
INSERT INTO `account_state` VALUES (-3,'Banned','Account Banned','Wirk Banned this Account'),(-2,'Suspended','Account Suspended','Wirk Suspended this Account'),(-1,'Deactivated','You Deactivated Account','Account has been Deactivated by User'),(0,'Inactive','Please Verify Email','Account has not yet been activated via Verification Email'),(1,'Active',NULL,NULL)$$

CREATE  TRIGGER `before_insert_notification` BEFORE INSERT ON `notification` FOR EACH ROW BEGIN DECLARE chat VARCHAR(255) DEFAULT TRIM(NEW.message); DECLARE img VARCHAR(32) DEFAULT TRIM(NEW.icon); DECLARE FOUND BOOLEAN DEFAULT TRUE; DECLARE INCR MEDIUMINT UNSIGNED DEFAULT 1; WHILE FOUND DO SET NEW.notif_count = INCR; SET FOUND = ((SELECT COUNT(notif_count) FROM notification WHERE notif_count = NEW.notif_count AND USER_ID = NEW.user_id) > 0); SET INCR = INCR + 1; END WHILE; SET NEW.message = IF(chat = '', \N, chat); SET NEW.icon = IF(img = '', \N, img); END$$

--
-- Dumping data for table `company`
--
INSERT INTO company VALUES(1, 'Illustrato', '40 Cape Road, Mill Park, Port Elizabeth'), (2, 'Dev Hub', '1  Hyde Park Lane, Hyde Park, Sandton'), (3, 'Batsamayi', '1  Hyde Park Lane, Waterfron, PE')$$

--
-- Dumping data for table `role`
--
INSERT INTO `role` VALUES ('A','Administrator',NULL),('M','Manager',NULL),('T','Tech',NULL),('G','Staff',NULL)$$

--
-- Dumping data for table `tokendesc`
--
INSERT INTO `tokendesc` VALUES ('EML','Email Reset','Used in verifying New Email Address'),('JWT','JSON Web Token','Used in Authentication'),('MFA','Multi-Factor Auth','Used for verification before being logged in'),('PSD','Password Reset Token','Used when user has Forgotten Password'),('REG','Account Activation','Used in verifying New Account'),('RMT','Remember Me Token','Used to Login without Credentials')$$

--
-- Dumping data for table `user`
--

INSERT INTO `user` VALUES 
('ZA224A000001','Tertuis','Gerber','9306045506088','tertuis.gerber@s4.co.za','0737045730','$2a$10$sAtaHFnb3oihiiiujOIWPumZRX8yk.lmv.mYtb1r1s9Httc0dQpAG',null,0,'1993-06-04','Ms.','ZA','2024-02-07 09:23:52','2024-02-07 09:23:52','A',1),
('ZA224A000002','Meggan','Maritz','9306045506088','meggan.maritz@s4.co.za','0737045769','$2a$10$fvsLP0igcfTJiBpLUHNyHOu9euUsdvOIbWIQouv46vx0mfNpXjsbO',null,0,'1993-06-04','Ms.','ZA','2024-02-07 09:24:20','2024-02-07 09:24:20','G',1),
('ZA224A000003','Daniel','Hendricks','9306045506088','daniel.hendricks@s4.co.za','0737145769','$2a$10$o8uKb4qfOuDE5/f0Vh9oE.4tEQf04QbvFyZDph8YXVCdjU2wBiYVq',null,0,'1993-06-04','Mr.','ZA','2024-02-07 09:29:01','2024-02-07 09:29:01','G',2),
('ZA224A000004','Evidence','Bangene','9306045506088','my@email.com','0737083737','$2a$10$Lu1.DuhqI485b5DIFUpqDuVQcEr.ldsAtzz6jFcOW7//YLWUVc7mC',null,0,'1993-06-04','Mr.','ZA','2024-01-15 10:55:03','2024-01-15 10:55:03','A',3),
('ZA224A000005','Michael','Corleon','9306045506088','super@email.com','0737045737','$2a$10$U3DBMT/iuZKxlepXR5vfqeu21nDE3AUJyW6WrkS5.8U.wHIPPPADi',null,0,'1993-06-04','Mr.','ZA','2024-01-15 11:04:33','2024-01-15 11:04:33','G',3),
('ZA224A000006','Sean','Michaels','9306045506088','alias@domain.tld','0737088780','$2a$10$Lu1.DuhqI485b5DIFUpqDuVQcEr.ldsAtzz6jFcOW7//YLWUVc7mC',null,0,'1990-04-01','Mrs.','ZA','2024-01-15 10:55:03','2024-01-15 10:55:03','M',2),
('ZA224A000007','Tony','Montana','9606045601087','coo@business.com','0745045737','$2a$10$U3DBMT/iuZKxlepXR5vfqeu21nDE3AUJyW6WrkS5.8U.wHIPPPADi',null,0,'1993-06-04','Mr.','ZA','2024-01-15 11:04:33','2024-01-15 11:04:33','G',1)$$

CREATE  TRIGGER `before_insert_user` BEFORE INSERT ON `user` FOR EACH ROW BEGIN DECLARE fname VARCHAR(20) DEFAULT TRIM(NEW.firstname); DECLARE lname VARCHAR(20) DEFAULT TRIM(NEW.lastname); DECLARE umail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.email)); DECLARE idno VARCHAR(32) DEFAULT TRIM(NEW.idnumber); DECLARE nation CHAR(2) DEFAULT UPPER(NEW.nationality); DECLARE FOUND BOOLEAN DEFAULT TRUE; DECLARE INCR MEDIUMINT UNSIGNED DEFAULT 1; WHILE FOUND DO SET NEW.user_id = CONCAT(nation,(SELECT (CONCAT(SUBSTRING(DATE_FORMAT(CURDATE(), '%Y'), 1, 1), DATE_FORMAT(CURDATE(), '%y')))), CASTE(NEW.idnumber, NEW.nationality), LPAD(INCR, 6, '0')); SET FOUND = ((SELECT COUNT(USER_ID) FROM user WHERE USER_ID = NEW.USER_ID) > 0); SET INCR = INCR + 1; END WHILE; SET @USER_ID = NEW.user_id; SET NEW.firstname = IF(fname = '', \N, fname); SET NEW.lastname = IF(lname = '', \N, lname); SET NEW.email = iF(umail = '', \N, umail); SET NEW.idnumber = IF(idno = '', \N, idno); SET NEW.nationality = IF(nation = '', \N, nation); SET NEW.dob = dob(idno, nation, NEW.dob); END$$

CREATE  TRIGGER `before_update_user` BEFORE UPDATE ON `user` FOR EACH ROW BEGIN DECLARE title VARCHAR(5) DEFAULT TRIM(NEW.title); DECLARE fname VARCHAR(20) DEFAULT TRIM(NEW.firstname); DECLARE lname VARCHAR(20) DEFAULT TRIM(NEW.lastname); DECLARE idno VARCHAR(32) DEFAULT TRIM(NEW.idnumber); DECLARE nation CHAR(2) DEFAULT UPPER(NEW.nationality); DECLARE umail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.email)); DECLARE uphone VARCHAR(15) DEFAULT TRIM(NEW.phone); DECLARE pic CHAR(5) DEFAULT TRIM(NEW.photo); SET title = IF(IFNULL(title, '') = '', OLD.title, title); SET fname = IF(IFNULL(fname, '') = '', OLD.firstname, fname); SET lname = IF(IFNULL(lname, '') = '', OLD.lastname, lname); SET nation = IF(IFNULL(nation, '') = '', OLD.nationality, nation); SET idno = IF(IFNULL(idno, '') = '', OLD.idnumber, idno); SET umail = IF(IFNULL(umail, '') = '', OLD.email, umail); SET uphone = IF(IFNULL(uphone, '') = '', OLD.phone, uphone); SET NEW.title = IF(title = '?', \N, title); SET NEW.firstname = IF(fname = '?', \N, fname); SET NEW.lastname = IF(lname = '?', \N, lname); SET NEW.idnumber = IF(idno = '?', OLD.idnumber, idno); SET NEW.nationality = IF(nation = '?', \N, nation); SET NEW.dob = IF(NEW.dob = CURRENT_DATE(), OLD.dob, dob(IF(idno = '?', OLD.idnumber, idno), IF(nation = '?', \N, nation), NEW.dob)); SET NEW.email = IF(umail = '?', \N, umail); SET NEW.phone = IF(uphone = '?', \N, uphone); SET NEW.photo = IF(pic = '', \N, pic); SET NEW.modified = NOW(); END$$
