export const environment = {
  production: true,
  appVersion: 'v0.0.1',
  api: 'http://localhost:9000',
  appPreviewDocsUrl: 'https://developmenthub.co.za',
  org: {
    name: 'DevelopmentHub',
    motto: 'We Make IT Happen',
  },
};
