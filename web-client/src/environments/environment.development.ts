export const environment = {
  production: false,
  appVersion: 'v0.0.1',
  api: 'http://192.168.0.175:9000',
  // api: 'http://localhost:9000',
  appPreviewDocsUrl: 'https://developmenthub.co.za',
  org: {
    name: 'DevelopmentHub',
    motto: 'We Make IT Happen',
  },
};
