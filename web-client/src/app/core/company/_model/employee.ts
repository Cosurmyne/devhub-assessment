import { BaseModel } from '@app/core/_base/crud';

export class EmployeeModel extends BaseModel {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  idnumber: string;
    constructor() {
        super();
        this.id = '';
    }
}
