import { BaseModel } from '@app/core/_base/crud';

export class CompanyModel extends BaseModel {
    id: number;
    name: string;
    address: string;
    constructor() {
        super();
        this.id = 0;
    }
}
