import { BaseModel } from '@app/core/_base/crud';

export class RoleModel extends BaseModel {
  id: number;
  name: string;
  description: string;
}
