// Angular
import { Injectable, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';

// RxJS
import { BehaviorSubject, Observable, Subject, of } from 'rxjs';
import { debounceTime, delay, switchMap, tap, map, catchError } from 'rxjs/operators';

// env
import { environment } from '@env/environment';

// Layout
import { SortColumn, SortDirection } from '@app/core/_base/layout';

// Models
import { EmployeeModel } from '..';

interface SearchResult {
    employees: EmployeeModel[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: SortColumn;
    sortDirection: SortDirection;
}

const compare = (v1: string | number | boolean, v2: string | number | boolean) => (v1 < v2 ? -1 : v1 > v2 ? 1 : 0);

function sort(employees: EmployeeModel[], column: SortColumn, direction: string): EmployeeModel[] {
    if (direction === '' || column === '') {
        return employees;
    } else {
        return [...employees].sort((a, b) => {
            console.log(a, b);
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function matches(employee: EmployeeModel, term: string, pipe: PipeTransform) {
    return (
        employee.email.toLowerCase().includes(term.toLowerCase()) ||
        pipe.transform(employee.firstname).includes(term) ||
        pipe.transform(employee.lastname).includes(term)
    );
}

@Injectable()
export class StaffService {
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _employees$ = new BehaviorSubject<EmployeeModel[]>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: '',
        sortColumn: '',
        sortDirection: '',
    };

    constructor(
        private $http: HttpClient,
        private $pipe: DecimalPipe
    ) {
        this._search$
            .pipe(
                tap(() => this._loading$.next(true)),
                debounceTime(200),
                switchMap(() => this._list()),
                delay(200),
                tap(() => this._loading$.next(false))
            )
            .subscribe(({ employees, total }) => {
                this._employees$.next(employees);
                this._total$.next(total);
            });

        this._search$.next();
    }

    get employees$() {
        return this._employees$.asObservable();
    }
    get total$() {
        return this._total$.asObservable();
    }
    get loading$() {
        return this._loading$.asObservable();
    }
    get page() {
        return this._state.page;
    }
    get pageSize() {
        return this._state.pageSize;
    }

    set page(page: number) {
        this._set({ page });
    }
    set pageSize(pageSize: number) {
        this._set({ pageSize });
    }
    set searchTerm(searchTerm: string) {
        this._set({ searchTerm });
    }
    set sortColumn(sortColumn: SortColumn) {
        this._set({ sortColumn });
    }
    set sortDirection(sortDirection: SortDirection) {
        this._set({ sortDirection });
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _list(): Observable<SearchResult> {
        let { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

        let list: Observable<SearchResult> = this.$http.get<SearchResult>(`${environment.api}/employee/ls/0/20`);
        list.subscribe(({ total, employees }) => {
            // 1. sort
            let sorted = sort(employees, sortColumn, sortDirection);

            // 2. filter
            sorted = sorted.filter((employee) => matches(employee, searchTerm, this.$pipe));

            // 3. paginate
            sorted = sorted.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
            return { total, employees: sorted };
        });
        return list;
    }

    crud(payload: EmployeeModel): Observable<any> {
        return this.$http.post(`${environment.api}/employee/crud`, payload).pipe(
            map((res: any) => {
                this._search$.next();
                return res;
            }),
            catchError((err) => {
                return of(false);
            })
        );
    }
    get(id: string): Observable<EmployeeModel> {
        return this.$http.get<EmployeeModel>(`${environment.api}/employee/id/${id}`);
    }
    rm(id: string): Observable<any> {
        return this.$http.delete(`${environment.api}/employee/${id}`).pipe(
            map((res: any) => {
                this._search$.next();
                return res;
            }),
            catchError((err) => {
                return of(false);
            })
        );
    }
}
