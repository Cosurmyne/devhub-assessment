// Angular
import { Injectable, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';

// RxJS
import { BehaviorSubject, Observable, Subject, of } from 'rxjs';
import { debounceTime, delay, switchMap, tap, catchError, map } from 'rxjs/operators';

// env
import { environment } from '@env/environment';

// Layout
import { SortColumn, SortDirection } from '@app/core/_base/layout';

// Models
import { CompanyModel } from '..';

interface SearchResult {
    companies: CompanyModel[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: SortColumn;
    sortDirection: SortDirection;
}

const compare = (v1: string | number | boolean, v2: string | number | boolean) => (v1 < v2 ? -1 : v1 > v2 ? 1 : 0);

function sort(companies: CompanyModel[], column: SortColumn, direction: string): CompanyModel[] {
    if (direction === '' || column === '') {
        return companies;
    } else {
        return [...companies].sort((a, b) => {
            console.log(a, b);
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function matches(company: CompanyModel, term: string, pipe: PipeTransform) {
    return (
        company.name.toLowerCase().includes(term.toLowerCase()) || pipe.transform(company.name).includes(term) || pipe.transform(company.address).includes(term)
    );
}

@Injectable()
export class OperationsService {
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _companies$ = new BehaviorSubject<CompanyModel[]>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: '',
        sortColumn: '',
        sortDirection: '',
    };

    constructor(
        private $http: HttpClient,
        private $pipe: DecimalPipe
    ) {
        this._search$
            .pipe(
                tap(() => this._loading$.next(true)),
                debounceTime(200),
                switchMap(() => this._list()),
                delay(200),
                tap(() => this._loading$.next(false))
            )
            .subscribe(({ companies, total }) => {
                this._companies$.next(companies);
                this._total$.next(total);
            });

        // this._search$.next();
    }

    get companies$() {
        return this._companies$.asObservable();
    }
    get total$() {
        return this._total$.asObservable();
    }
    get loading$() {
        return this._loading$.asObservable();
    }
    get page() {
        return this._state.page;
    }
    get pageSize() {
        return this._state.pageSize;
    }

    set page(page: number) {
        this._set({ page });
    }
    set pageSize(pageSize: number) {
        this._set({ pageSize });
    }
    set searchTerm(searchTerm: string) {
        this._set({ searchTerm });
    }
    set sortColumn(sortColumn: SortColumn) {
        this._set({ sortColumn });
    }
    set sortDirection(sortDirection: SortDirection) {
        this._set({ sortDirection });
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _list(): Observable<SearchResult> {
        let { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

        let list: Observable<SearchResult> = this.$http.get<SearchResult>(`${environment.api}/company/ls/0/20`);
        list.subscribe(({ total, companies }) => {
            // 1. sort
            let sorted = sort(companies, sortColumn, sortDirection);

            // 2. filter
            sorted = sorted.filter((company) => matches(company, searchTerm, this.$pipe));

            // 3. paginate
            sorted = sorted.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
            return { total, companies: sorted };
        });
        return list;
    }

    crud(payload: CompanyModel): Observable<any> {
        return this.$http.post(`${environment.api}/company/crud`, payload).pipe(
            map((res: any) => {
                this._search$.next();
                return res;
            }),
            catchError((err) => {
                return of(false);
            })
        );
    }
    get(id: number): Observable<CompanyModel> {
        return this.$http.get<CompanyModel>(`${environment.api}/company/id/${id}`);
    }
    rm(id: number): Observable<any> {
        return this.$http.delete(`${environment.api}/company/${id}`).pipe(
            map((res: any) => {
                this._search$.next();
                return res;
            }),
            catchError((err) => {
                return of(false);
            })
        );
    }
}
