// Angular
import { Injectable } from '@angular/core';
import { TitleStrategy, RouterStateSnapshot } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ViewportScroller } from '@angular/common';

// Environment
import { environment } from '@env/environment';

@Injectable()
export class TitleService extends TitleStrategy {
    constructor(
        private viewportScroller: ViewportScroller,
        private readonly title: Title
    ) {
        super();
    }

    override updateTitle(routerState: RouterStateSnapshot) {
        this.viewportScroller.scrollToPosition([0, 0]);
        let title = this.buildTitle(routerState);
        if (title !== undefined) {
            this.title.setTitle(`DevHub | ${title}`);
            return;
        }
        this.title.setTitle(`DevHub | ${environment.org.motto}`);
    }
}

