// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DecimalPipe } from '@angular/common';

// Layout
import { SortableHeaderDirective } from './_base/layout';

@NgModule({
    declarations: [SortableHeaderDirective],
    imports: [CommonModule, DecimalPipe],
    exports: [SortableHeaderDirective],
    providers: [DecimalPipe],
})
export class CoreModule {}
