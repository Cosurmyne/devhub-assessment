// Angular
import { NgModule } from '@angular/core';
import { TitleStrategy } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Layout Services
import {
  LayoutConfigService,
  LayoutInitService,
  PageInfoService,
  TitleService,
} from './core/_base/layout';

// modules
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from './core/module';
import { ThemeModule } from './view/theme/module';

//Components
import { AppComponent } from './app.component';
import { ScriptsInitComponent } from './_lib/scripts-init.component';
import { PartialModule } from './view/partial/module';

@NgModule({
  declarations: [AppComponent, ScriptsInitComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    CoreModule,
    ThemeModule,
    PartialModule,
  ],
  providers: [
    LayoutConfigService,
    LayoutInitService,
    PageInfoService,
    { provide: TitleStrategy, useClass: TitleService },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
