import { Component } from '@angular/core';

@Component({
  selector: 'body[app-root]',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'web-client';
}
