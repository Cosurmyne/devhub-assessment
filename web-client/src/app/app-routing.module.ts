// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { ThemeComponent } from './view/theme/theme.component';

const routes: Routes = [
  {
    path: 'error',
    loadChildren: () =>
      import('./view/page/error/module').then((m) => m.ErrorModule),
  },
  {
    path: '',
    component: ThemeComponent,
    children: [
      {
        path: 'org',
        loadChildren: () =>
          import('./view/page/company/module').then((m) => m.CompanyModule),
      },
      {
        path: 'staff',
        loadChildren: () =>
          import('./view/page/employee/module').then((m) => m.EmployeeModule),
      },
      { path: '', redirectTo: 'org', pathMatch: 'full' },
      { path: '**', redirectTo: 'org', pathMatch: 'full' },
    ],
  },
  { path: '**', redirectTo: 'error/404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
