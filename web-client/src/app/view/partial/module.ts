// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetModule } from './content/widget/module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WidgetModule
  ]
})
export class PartialModule { }
