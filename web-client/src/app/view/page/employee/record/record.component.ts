// Angular
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

// RxJS
import { Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';

// Services + Models
import { StaffService, EmployeeModel } from '@app/core/company';

@Component({
    selector: 'page-record',
    templateUrl: './record.component.html',
    styleUrl: './record.component.scss',
})
export class RecordComponent implements OnInit {
    // Public Fields
    details: FormGroup;
    loading = false;
    errors: any = [];

    // Private Fields
    private _unsub: Subject<any>; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
    private _record: EmployeeModel = new EmployeeModel();

    /**
     * Component constructor
     *
     * @param $route	: ActivatedRoute
     * @param $router: Router
     * @param $fb: FormBuilder
     * @param $cdr
     * @param $staff: StaffService
     */
    constructor(
        private $route: ActivatedRoute,
        private $router: Router,
        private $fb: FormBuilder,
        private $cdr: ChangeDetectorRef,
        private $staff: StaffService
    ) {
        $route.params.subscribe(({ id }) => {
            if (id) {
                this._record.id = id;
                $staff.get(id).subscribe(this.initForm);
            }
        });
        this._unsub = new Subject();
    }

    /*
     * On Init
     */
    ngOnInit() {
        this.initForm(new EmployeeModel());
    }

    /**
     * Form initalization
     * Default params, validators
     */
    initForm = ({ firstname, lastname, email, idnumber }: EmployeeModel) => {
        this.details = this.$fb.group({
            firstname: [firstname, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
            lastname: [lastname, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
            email: [email, Validators.compose([Validators.required, Validators.email, Validators.minLength(3), Validators.maxLength(320)])],
            idnumber: [idnumber, Validators.compose([Validators.required, Validators.minLength(13), Validators.maxLength(13)])],
        });
    };

    async onSave() {
        const controls = this.details.controls;

        // check form
        if (this.details.invalid) {
            Object.keys(controls).forEach((controlName) => controls[controlName].markAsTouched());
            return;
        }

        this.loading = true;

        this._record.firstname = controls['firstname'].value;
        this._record.lastname = controls['lastname'].value;
        this._record.email = controls['email'].value;
        this._record.idnumber = controls['idnumber'].value;

        this.$staff
            .crud(this._record)
            .pipe(
                tap((res) => {
                    if (res) {
                        // pass notice message to the login page
                        this.$router.navigateByUrl('/staff');
                    } else {
                        alert('Failure');
                    }
                }),
                takeUntil(this._unsub),
                finalize(() => {
                    this.loading = false;
                    this.$cdr.markForCheck();
                })
            )
            .subscribe();
    }

    /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.details.controls[controlName];
        if (!control) {
            return false;
        }

        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }
}
