// Angular
import { Component, QueryList, ViewChildren } from '@angular/core';

// RxJS
import { Observable } from 'rxjs';

// Layout
import { SortableHeaderDirective, SortEvent } from '@app/core/_base/layout';

// Services + Models
import { StaffService, EmployeeModel } from '@app/core/company';

@Component({
    selector: 'page-index',
    templateUrl: './index.component.html',
    styleUrl: './index.component.scss',
})
export class IndexComponent {
    employees$: Observable<EmployeeModel[]>;
    total$: Observable<number>;

    @ViewChildren(SortableHeaderDirective) headers: QueryList<SortableHeaderDirective>;

    /**
     * Constructor
     *
     * @param $staff : StaffService
     */
    constructor(private $staff: StaffService) {
        this.employees$ = $staff.employees$;
        this.total$ = $staff.total$;
    }

    onSort({ column, direction }: SortEvent) {
        // resetting other headers
        this.headers.forEach((header) => {
            if (header.sortable !== column) {
                header.direction = '';
            }
        });

        this.$staff.sortColumn = column;
        this.$staff.sortDirection = direction;
    }

    onDelete(id: string) {
        this.$staff.rm(id).subscribe();
    }
}
