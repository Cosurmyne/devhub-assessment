// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Modules
import { CoreModule } from '@app/core/module';
import { PartialModule } from '@app/view/partial/module';
import { WidgetModule } from '@app/view/partial/content/widget/module';

// Services
import { StaffService } from '@app/core/company';

// Components
import { RecordComponent } from './record/record.component';
import { IndexComponent } from './index/index.component';

@NgModule({
    declarations: [RecordComponent, IndexComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                title: 'Staff',
                component: IndexComponent,
            },
            {
                path: 'add',
                title: 'New Member',
                component: RecordComponent,
            },
            {
                path: 'update/:id',
                title: 'Maintain Info',
                component: RecordComponent,
            },
        ]),
        FormsModule,
        ReactiveFormsModule,
        CoreModule,
        PartialModule,
        WidgetModule,
    ],
    providers: [StaffService],
})
export class EmployeeModule {}
