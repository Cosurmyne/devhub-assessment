// Angular
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-error',
  templateUrl: './error.component.html',
  styleUrl: './error.component.scss',
})
export class ErrorComponent {
  // Public Fields
  code: number;
  message: string;

  // Private Fields
  private _codes: { [key: number]: string } = {
    200: 'OK',
    201: 'Created',
    202: 'Accepted',
    301: 'Moved Permanently',
    302: 'Found',
    307: 'Temporary Redirect',
    308: 'Permanent Redirect',
    400: 'Bad Request',
    401: 'Unauthorized',
    403: 'Access Forbidden',
    404: 'Page Not Found',
    500: 'Internal Server Error',
    501: 'Not Implemented',
    502: 'Bad Gateway',
  };

  /**
   * Component constructor
   *
   * @param $router: Router
   * @param $subheader: SubheaderService
   */
  constructor(private $route: ActivatedRoute) {
    $route.params.subscribe(({ code }) => {
      this.code = code;
      this.message = this._codes[code];
    });
  }
}
