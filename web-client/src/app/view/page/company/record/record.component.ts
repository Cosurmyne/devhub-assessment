// Angular
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

// RxJS
import { Subject } from 'rxjs';
import { tap, takeUntil, finalize } from 'rxjs/operators';

// Services + Models
import { OperationsService, CompanyModel } from '@app/core/company';

@Component({
    selector: 'page-record',
    templateUrl: './record.component.html',
    styleUrl: './record.component.scss',
})
export class RecordComponent implements OnInit {
    // Public Fields
    details: FormGroup;
    loading = false;
    errors: any = [];

    // Private Fields
    private _unsub: Subject<any>; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
    private _record: CompanyModel = new CompanyModel();

    /**
     * Component constructor
     *
     * @param $route	: ActivatedRoute
     * @param $router: Router
     * @param $fb: FormBuilder
     * @param $cdr
     * @param $org: OperationsService
     */
    constructor(
        private $route: ActivatedRoute,
        private $router: Router,
        private $fb: FormBuilder,
        private $cdr: ChangeDetectorRef,
        private $org: OperationsService
    ) {
        $route.params.subscribe(({ id }) => {
            if (id) {
                this._record.id = id;
                $org.get(id).subscribe(this.initForm);
            }
        });
        this._unsub = new Subject();
    }

    /**
     * On init
     */
    ngOnInit() {
        this.initForm(new CompanyModel());
    }

    /**
     * Form initalization
     * Default params, validators
     */
    initForm = ({ name, address }: CompanyModel) => {
        this.details = this.$fb.group({
            name: [name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(45)])],
            address: [address, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
        });
    };

    async onSave() {
        const controls = this.details.controls;

        // check form
        if (this.details.invalid) {
            Object.keys(controls).forEach((controlName) => controls[controlName].markAsTouched());
            return;
        }

        this.loading = true;

        this._record.name = controls['name'].value;
        this._record.address = controls['address'].value;

        this.$org
            .crud(this._record)
            .pipe(
                tap((res) => {
                    if (res) {
                        // pass notice message to the login page
                        this.$router.navigateByUrl('/org');
                    } else {
                        alert('Failure');
                    }
                }),
                takeUntil(this._unsub),
                finalize(() => {
                    this.loading = false;
                    this.$cdr.markForCheck();
                })
            )
            .subscribe();
    }

    /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.details.controls[controlName];
        if (!control) {
            return false;
        }

        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }
}
