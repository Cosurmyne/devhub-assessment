// Angular
import { Component, QueryList, ViewChildren, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// RxJS
import { Observable } from 'rxjs';

// Layout
import { SortableHeaderDirective, SortEvent } from '@app/core/_base/layout';

// Services + Models
import { OperationsService, CompanyModel } from '@app/core/company';

@Component({
    selector: 'page-index',
    templateUrl: './index.component.html',
    styleUrl: './index.component.scss',
})
export class IndexComponent implements OnInit {
    companies$: Observable<CompanyModel[]>;
    total$: Observable<number>;

    @ViewChildren(SortableHeaderDirective) headers: QueryList<SortableHeaderDirective>;

    /**
     * Constructor
     *
     * @param $operations : OperationsService
     */
    constructor(private $org: OperationsService, private $route: ActivatedRoute) {
        this.companies$ = $org.companies$;
        this.total$ = $org.total$;
	 $route.params.subscribe(({ service }) => {
        this.$org.page = 1;
        });
    }

    ngOnInit() {
    }

    onSort({ column, direction }: SortEvent) {
        // resetting other headers
        this.headers.forEach((header) => {
            if (header.sortable !== column) {
                header.direction = '';
            }
        });

        this.$org.sortColumn = column;
        this.$org.sortDirection = direction;
    }

    onDelete(id: number) {
        this.$org.rm(id).subscribe();
    }
}
