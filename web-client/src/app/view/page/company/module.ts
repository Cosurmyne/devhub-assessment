// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Modules
import { CoreModule } from '@app/core/module';
import { PartialModule } from '@app/view/partial/module';
import { WidgetModule } from '@app/view/partial/content/widget/module';

// Services
import { OperationsService } from '@app/core/company';

// Components
import { IndexComponent } from './index/index.component';
import { RecordComponent } from './record/record.component';

@NgModule({
    declarations: [RecordComponent, IndexComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                title: 'List',
                component: IndexComponent,
            },
            {
                path: 'add',
                title: 'Register New',
                component: RecordComponent,
            },
            {
                path: 'update/:id',
                title: 'Maintain Info',
                component: RecordComponent,
            },
        ]),
        FormsModule,
        ReactiveFormsModule,
        CoreModule,
        PartialModule,
        WidgetModule,
    ],
    providers: [OperationsService],
})
export class CompanyModule {}
