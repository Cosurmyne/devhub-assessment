import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';

@Component({
  selector: 'sidebar-footer',
  templateUrl: './sidebar-footer.component.html',
})
export class SidebarFooterComponent implements OnInit {
  appPreviewChangelogUrl: string = environment.appPreviewDocsUrl;

  constructor() {}

  ngOnInit(): void {}
}
