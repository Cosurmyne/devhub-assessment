// Angular
import { Component, Input, OnDestroy, OnInit } from '@angular/core';

// RxJS
import { Subscription } from 'rxjs';

import { LayoutType } from '@app/core/_config/layout';
import { LayoutConfigService } from '@app/core/_base/layout';

@Component({
  selector: 'sidebar-logo',
  templateUrl: './sidebar-logo.component.html',
})
export class SidebarLogoComponent implements OnInit, OnDestroy {
  private unsubscribe: Subscription[] = [];
  @Input() toggleButtonClass: string = '';
  @Input() toggleEnabled: boolean;
  @Input() toggleType: string = '';
  @Input() toggleState: string = '';
  currentLayoutType: LayoutType | null;

  toggleAttr: string;

  constructor(private $layout: LayoutConfigService) {}

  ngOnInit(): void {
    this.toggleAttr = `app-sidebar-${this.toggleType}`;
    const layoutSubscr = this.$layout.currentLayoutTypeSubject
      .asObservable()
      .subscribe((layout) => {
        this.currentLayoutType = layout;
      });
    this.unsubscribe.push(layoutSubscr);
  }

  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}
