// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PartialModule } from '@app/view/partial/module';
import { WidgetModule } from '@app/view/partial/content/widget/module';

// Component
import { ContentComponent } from './content/content.component';
import { ThemeComponent } from './theme.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TopbarComponent } from './topbar/topbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarFooterComponent } from './sidebar/sidebar-footer/sidebar-footer.component';
import { SidebarLogoComponent } from './sidebar/sidebar-logo/sidebar-logo.component';
import { SidebarMenuComponent } from './sidebar/sidebar-menu/sidebar-menu.component';
import { PageTitleComponent } from './header/page-title/page-title.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
  declarations: [
    ContentComponent,
    ThemeComponent,
    HeaderComponent,
    FooterComponent,
    TopbarComponent,
    SidebarComponent,
    SidebarFooterComponent,
    SidebarLogoComponent,
    SidebarMenuComponent,
    PageTitleComponent,
    ToolbarComponent,
  ],
  imports: [CommonModule, RouterModule, PartialModule, WidgetModule],
  exports: [
    ContentComponent,
    ThemeComponent,
    HeaderComponent,
    FooterComponent,
    TopbarComponent,
    SidebarComponent,
    SidebarFooterComponent,
    SidebarLogoComponent,
    SidebarMenuComponent,
    PageTitleComponent,
    ToolbarComponent,
  ],
})
export class ThemeModule {}
