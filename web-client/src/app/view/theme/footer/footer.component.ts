import { Component, Input } from '@angular/core';

@Component({
  selector: 'theme-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent {
  @Input() appFooterContainerCSSClass: string = '';

  currentDateStr: string = new Date().getFullYear().toString();
  constructor() {}
}
